# Crowdsec

Crowdsec est un IPS collaboratif permettant de bloquer les attaques en direction de vos serveurs. Dans mon cas, il est déployé sous Docker et fonctionne avec Traefik sur mes serveurs. En fonction des signatures ou des adresses IP connues, il bloquera le trafic et enverra un rapport d'erreur à la console et à la base de données en ligne de Crowdsec.

(En cas de ban, les adresses sont débloquées au bout de 4 h par défaut)

# Fonctionnement

Crowdsec fonctionne à l'aide de deux composants : le conteneur crowdsec qui prends les décisions de blocage, et en averti le bouncer qui interagit avec Traefik pour faire valoir le blocage.

# Architecture du repository

Je me suis aidé de l'excellente vidéo de Techno Tim pour le déploiement : https://www.youtube.com/watch?v=-GxUP6bNxF0&t=1476s.

Vous trouverez ce dépôt le docker-compose pour déployer les composants crowdsec nécessaires, et tous les éléments de configuration dans le dossier `config` pour Traefik et l'instance Crowdsec.

# Installation

Je ferais la documentation d'installation dans un futur proche.